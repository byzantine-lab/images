#/bin/bash

CWD="$(dirname "$0")"

for i in $(ls $CWD); do
  if [ -d $i ]; then
    echo "Building $i ..."
    docker build -t registry.gitlab.com/byzantine-lab/images/$i:latest $i
    docker push registry.gitlab.com/byzantine-lab/images/$i:latest
  fi
done
